% Copyright 2020 by Benoît Vaillant
%
% This file may be distributed and/or modified under
% the LaTeX Project Public License 1.3c, or (at your
% option) any later version.
% 
% Note: work heavily based on twentysecondcv package available at:
% https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex/
% which is licenced under the MIT licence. The original restrictions
% thus apply to these parts.
% 
% See the file LICENSE for more details.

\ProvidesClass{tscvng}[2018/11/19 CV class]
\NeedsTeXFormat{LaTeX2e}

%% language option
\def\langoption{french} %% default
%% other possible languages
\DeclareOption{english}{
  \def\langoption{english}
}
\PassOptionsToClass{\langoption}{article}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%PACKAGES%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[sfdefault]{ClearSans}
\RequirePackage[T1]{fontenc}
\RequirePackage{tikz}
\RequirePackage{xcolor}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{ragged2e}
\RequirePackage{marvosym}
\RequirePackage{parskip}

\RequirePackage[\langoption]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{pstricks,pst-text,pst-plot}
\RequirePackage{colortbl}
\RequirePackage{amsmath}
\RequirePackage{enumitem}
\RequirePackage{wasysym}
\RequirePackage{pifont}
\RequirePackage{turnstile}
\RequirePackage{fontawesome}
\RequirePackage{diagbox}
\RequirePackage[most]{tcolorbox}
\RequirePackage[nodayofweek]{datetime}

%% maths and other symbols
\RequirePackage{amssymb}
\RequirePackage{fourier}

\RequirePackage{lettrine}

\RequirePackage[hidelinks]{hyperref}
%% \hypersetup{%
%%   colorlinks=false,% hyperlinks will be black
%%   linkbordercolor=red,% hyperlink borders will be red
%%   pdfborderstyle={/S/U/W 1}% border style will be underline of width 1pt
%% }


%% must be loaded _after_ hyperref
\RequirePackage[left=7.3cm,top=0.1cm,right=0.5cm,bottom=0.2cm,nohead,nofoot]{geometry}
\RequirePackage{multimedia}

%% fancy things
\usetikzlibrary{positioning}

\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{article}
}
\ProcessOptions\relax

%%%%%%%%%%
% Colors %
%%%%%%%%%%
\definecolor{white}{RGB}{255,255,255}

\definecolor{darkgray}{HTML}{333333}
\definecolor{gray}{HTML}{4D4D4D}
\definecolor{sidecolor}{HTML}{E7E7E7}
\definecolor{lightgray}{HTML}{999999}

\definecolor{green}{HTML}{C2E15F}
\definecolor{orange}{HTML}{FDA333}
\definecolor{purple}{HTML}{D3A4F9}
\definecolor{red}{HTML}{FB0B00}
\definecolor{blue}{HTML}{6CE0F1}
\definecolor{mainblue}{HTML}{0E5484}
\definecolor{maingray}{HTML}{B9B9B9}
\definecolor{maindarkgray}{HTML}{B3B3B3}
\ifdefined\@cv@print
  \colorlet{green}{gray}
  \colorlet{orange}{gray}
  \colorlet{purple}{gray}
  \colorlet{red}{gray}
  \colorlet{blue}{gray}
  \colorlet{fillheader}{white}
  \colorlet{asidecolor}{sidecolor}
  \colorlet{header}{gray}
\else
  \colorlet{fillheader}{gray}
  \colorlet{header}{white}
  \colorlet{asidecolor}{sidecolor}
\fi

\definecolor{BleuClair}{RGB}{163, 181, 250}
\definecolor{Vert}{RGB}{47, 163, 69}

\colorlet{textcolor}{gray}
\colorlet{headercolor}{gray}

%%%%%%%%%%%%%%%%%%%%%%%%
% Profile left SideBar %
%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}

\newcommand{\imsize}{\linewidth}
\newlength\imagewidth
\newlength\imagescale
\renewcommand{\imsize}{0.618\linewidth}
\pgfmathsetlength{\imagewidth}{5cm}%
\pgfmathsetlength{\imagescale}{\imagewidth/600}%


\newcommand{\profilesection}[2]{{\noindent\color{black!80} \Large #1 \rule[0.15\baselineskip]{#2}{1pt} \vspace{1pt}}}


\newcommand{\cvdate}[1]{\renewcommand{\givencvdate}{#1}}
\newcommand{\cvmail}[1]{\renewcommand{\givencvmail}{#1}}
\newcommand{\cvnumberphone}[1]{\renewcommand{\givennumberphone}{#1}}
\newcommand{\cvaddress}[1]{\renewcommand{\givencvaddress}{#1}}
\newcommand{\cvsite}[1]{\renewcommand{\givencvsite}{#1}}
\newcommand{\aboutme}[1]{\renewcommand{\givenaboutme}{  \justifying#1}}
\newcommand{\profilepic}[1]{\renewcommand{\givenprofilepic}{#1}}
\newcommand{\cvname}[1]{\renewcommand{\givencvname}{#1}}
\newcommand{\cvjobtitle}[1]{\renewcommand{\givencvjobtitle}{#1}}
\newcommand{\cvwarning}[1]{\renewcommand{\givencvwarning}{#1}}
\newcommand{\cvsearch}[1]{\renewcommand{\givencvsearch}{#1}}
\newcommand{\cvmobility}[1]{\renewcommand{\givencvmobility}{#1}}

\newcommand{\givencvname}{REQUIRED!}
\newcommand{\givencvdate}{}
\newcommand{\givencvmail}{}
\newcommand{\givennumberphone}{}
\newcommand{\givencvaddress}{} 
\newcommand{\givenaboutme}{}
\newcommand{\givenprofilepic}{}
\newcommand{\givencvsite}{}
\newcommand{\givencvjobtitle}{}
\newcommand{\givencvwarning}{}
\newcommand{\givencvsearch}{}
\newcommand{\givencvmobility}{}

\newcommand*\icon[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=1pt, fill=mainblue,mainblue,text=white] (char) {#1};}}
\newcommand*\round[2]{%
  \tikz[baseline=(char.base)]\node[anchor=north west, draw,rectangle, rounded corners, inner sep=1.6pt, minimum size=5.5mm,
    text height=3.6mm,  fill=#2,#2,text=white](char){#1} ;}

\newcommand\skills[1]{ 

\renewcommand{\givenskill}{
\noindent
\begin{tikzpicture}
\foreach [count=\i] \x/\y in {#1}
{

\draw[fill=maingray,maingray] (0,\i) rectangle (6,\i+0.4);
\draw[fill=white,mainblue](0,\i) rectangle (\y,\i+0.4);
\node [above right ] at (0,\i+0.4) {\x$~\star $\y};
}
\end{tikzpicture}
}
}
\newcommand{\givenskill}{}

\newcommand\skillstext[1]{ 

\renewcommand{\giventextskill}{
\begin{flushleft}
\noindent
\foreach [count=\i] \x/\y in {#1}
{ 
\x$~\star $\y
}
\end{flushleft}
}
}

\newcommand{\giventextskill}{}
  
\newcommand{\makeprofile}
{
\begin{tikzpicture}[remember picture,overlay]
   	 \node [rectangle, fill=asidecolor, anchor=north, minimum width=9.90cm, minimum height=\paperheight+1cm] (box) at (-5cm,0.5cm){};
\end{tikzpicture}

\begin{textblock}{6}(0.5, 0.2)
  \begin{flushleft}
    \hspace{1.9cm}
    \resizebox{2cm}{2cm}{%
      \begin{tikzpicture}[x=\imagescale,y=-\imagescale]
        \clip (600/2, 567/2) circle (567/2);
        \node[anchor=north west, inner sep=0pt, outer sep=0pt] at (0,0) {  
          \includegraphics[width=\imagewidth]{\givenprofilepic}
        };
      \end{tikzpicture}
    }
    \newline
    \newline
{\Huge\color{mainblue}\givencvname}

\begin{flushright}
{\Large\color{black!80}\givencvjobtitle}
\end{flushright}

\renewcommand{\arraystretch}{1.6}
\begin{tabular}{p{0.5cm} @{\hskip 0.5cm}p{5cm}}
\ifthenelse{\equal{\givencvdate}{}}{}{\textsc{\Large\icon{\Info}} & \givencvdate\\}
\ifthenelse{\equal{\givencvaddress}{}}{}{\textsc{\Large\icon{\Letter}} & \givencvaddress\\}
\ifthenelse{\equal{\givennumberphone}{}}{}{\textsc{\Large\icon{\Telefon}} & \givennumberphone\\}
%\ifthenelse{\equal{\givencvsite}{}}{}{\textsc{\Large\icon{\Earth}} & \href{\givencvsite}}{\givencvsite}\\
\ifthenelse{\equal{\givencvmail}{}}{}{\textsc{\large\icon{@}} & \href{mailto:\givencvmail}{\givencvmail}\\}
\ifthenelse{\equal{\givencvwarning}{}}{}{\textsc{\large\icon{\danger}} & \givencvwarning\\}
\ifthenelse{\equal{\givencvsearch}{}}{}{\textsc{\icon{\faSearch}} & \givencvsearch\\}
\ifthenelse{\equal{\givencvmobility}{}}{}{\textsc{\icon{\faSubway}} & \givencvmobility\\}
\end{tabular}

\profilesection{\vfill \hspace{0.4cm}Profil}{4.2cm}
\givenaboutme

\profilesection{\vfill \hspace{-0.1cm}Aptitudes}{3.4cm}
\givenskill
\giventextskill
\noindent
\scriptsize
\noindent
[\'Echelle de 0 (Base) \`a 6 (Expert).]

\end{flushleft}
\end{textblock}
\vspace{-10pt}
 }

%%%%%%%%%%%%%%%%
% Section Color box %
%%%%%%%%%%%%%%%%
\newcounter{colorCounter}
%\def\@sectioncolor#1#2#3{%
\def\@sectioncolor#1{%
  {%
%   \round{#1#2#3}{
   \round{#1}{
    \ifcase\value{colorCounter}%
        maingray\or%
        mainblue\or%
        maingray\or%
        mainblue\or%
        maingray\or%
        mainblue\or%
        maingray\or%
        mainblue\else%
        maingray\fi%
    }%
  }%
  \stepcounter{colorCounter}%
}

%% new section definition
\def\ificonsizeempty#1{
  \def\sectionicontmp{#1} \ifx\sectionicontmp\empty
}
\newlength\sectioniconheight
\def\sectioniconmacro#1{  \ificonsizeempty{#1} \setlength{\sectioniconheight}{1.2\baselineskip} \else \setlength{\sectioniconheight}{#1\baselineskip} \fi }

\renewcommand{\section}[5][white]{
  \sectioniconmacro{#5}
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}
      \draw[-, > = stealth] (0, 0) -- (2.5, 0) ;
      \draw[-, > = stealth] (1.5, 0) -- (11, 0) node [midway, fill = #1] {#2 [~#3~]} ;
      \draw[-, > = stealth] (11, 0) -- (11.5, 0) node [midway, fill = #1] {#2 ~\resizebox{\sectioniconheight}{!}{#4}~} ;
      \draw[-, > = stealth] (11.8, 0) -- (12.5, 0) ;
    \end{tikzpicture}
  }
  \par\vspace{-.25\parskip}%
}

\renewcommand{\subsection}[1]{
  \par\vspace{.5\parskip}%
  {%
  \large\color{headercolor} #1%
  }
  \par\vspace{.25\parskip}%
}

\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%
% List twenty environment %
%%%%%%%%%%%%%%%%%%%%

\setlength{\tabcolsep}{0pt}
\newenvironment{twenty}{%
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lll}
}{%
  \end{tabular*}
}

\renewcommand{\bfseries}{\color{headercolor}}
\newcommand{\twentyitem}[6]{%
  \hspace{5pt}#1&\hspace{10pt}\parbox[t]{.8\textwidth}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize#3}\\%
    {\raisebox{-0.15ex}{\small\it#4}}
    \hfill%
    {\footnotesize#5}
    \vspace{\parsep}%  
  }
  \vspace{-0.4cm}%
  \hspace{0.2cm}%
  #6%
  %\vspace{0.1cm}
  \\}

\newcommand{\twentyitemlong}[6]{%
  \hspace{5pt}#1&\parbox[t]{.85\textwidth}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize#3}\\%
    {\raisebox{-0.15ex}{\small\it#4}}
    \hfill%
    {\footnotesize#5}
    \hfill%
    \vspace{\parsep}%  
  }
  \vspace{-0.5cm}%
  #6%
  \\}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Small twenty List environment %
%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\tabcolsep}{0pt}
\newenvironment{twentyshort}{%
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lll}
}{%
  \end{tabular*}
}
\renewcommand{\bfseries}{\color{headercolor}}
\newcommand{\twentyitemshort}[2]{%
  \hspace{5pt}#1&\parbox[t]{11.5cm}{%
    \textbf{#2}%
  }\\}


%%%%%%%%%%%%%%%%
% Page Setting %
%%%%%%%%%%%%%%%%

%% allow using custom files for specific CVs
\newcommand{\custominput}[1]{
  \IfFileExists{#1}{\input{#1}}{\input{../../#1}}
}

\newcommand\customincludegraphics[2][]{\IfFileExists{#2}
  {\includegraphics[#1]{#2}}%
  {\includegraphics[#1]{../../#2}}%
}


