# tscvng makefile

INPUTS=$(wildcard *.tex)
NESTED_TARGETS=CVs/rambo

all:
	@echo "Building all, this may take some time!"
	@$(foreach var, $(INPUTS), $(MAKE) $(var:.tex=.pdf);)
ifneq ($(strip $(NESTED_TARGETS)),)
	@$(MAKE) $(NESTED_TARGETS)
endif
	@echo "Done all!"

%.pdf: %.tex
	@echo "Building.... $<"
	@pdflatex -interaction=nonstopmode -jobname=$(@:.pdf=) '$<' 1>/dev/null
	@echo "Done!"

distclean:
	@$(foreach var, $(shell find . -type f -name 'Makefile'), $(MAKE) -C $(dir $(var)) clean;)
	@echo "Distclean done!"

clean: FORCE
	@rm -f *.aux *.dvi *.log *.out
	@rm -f *.idx *.bcf *.ilg *.ind *.lof *.lot *.toc *.run.xml
	@rm -f *.bbl *.blg *.ind
	@find . -type f -name "*~" -print0 | xargs -0 rm -f
	@echo "Clean done."

$(NESTED_TARGETS): FORCE
	@echo "Nesting into.... $@"
	@cd $@ && $(MAKE)

FORCE:
